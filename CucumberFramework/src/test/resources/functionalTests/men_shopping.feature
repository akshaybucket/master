@Current
Feature: Add Tshirt to cart
Description: This feature file is about testing add TShirt to cart with differnt scenarios

Background:	
	Given user is on Home Page
	And I enter username and password and clicks on sign-in button
	
Scenario: 
	#Given I click on TShirt tab
	When I click on Quick view for first tshirt
	And I select quantity as '3' size as 'M' and color 'Blue'
	And I click on add to cart button
	And I click on procced to checkout button
	Then Shopping cart page should be displayed  
	
	
#Feature: Men Shopping
#Description: The purpose of this feature is to test Mens Shoppings feature.
	
#@Current
#Scenario: Customer place an order by purchasing an item from search
	#When he clicks on Mens tab
	#And Product category is "Pants & Shirts"
	#And color is "Brown" and size is "34" 
	
