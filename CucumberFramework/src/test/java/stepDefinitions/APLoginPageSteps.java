package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import pageObjects.APLoginPage;

public class APLoginPageSteps {
	TestContext testContext;
	APLoginPage apLoginPage;
	
	public APLoginPageSteps(TestContext context) {
		testContext = context;
		apLoginPage = testContext.getPageObjectManager().getAPLoginPage();
	}
	
	@Given("^I am on Sign-in page$")
	public void i_am_on_Sign_in_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		apLoginPage.navigateTo_LoginPage();
	}
	

	/*@Given("^user is on Home Page$")
	public void user_is_on_Home_Page(){
		homePage.navigateTo_HomePage();	
	}

	@When("^he search for \"([^\"]*)\"$")
	public void he_search_for(String product)  {
		homePage.perform_Search(product);
	}

	@When("^he clicks on Mens tab$")
	public void he_clicks_on_Mens_tab(){
		homePage.clickOn_MensTab();
	}*/

}
