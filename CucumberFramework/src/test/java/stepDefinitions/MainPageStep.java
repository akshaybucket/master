package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import enums.Context;
import pageObjects.APHomePage;


public class MainPageStep {
	
	TestContext testContext;
	APHomePage aphomepage;
	
	public MainPageStep(TestContext context) {
		testContext = context;
		aphomepage = testContext.getPageObjectManager().getAPHomePage();
	}

	@Given("I click on TShirt tab")
	public void ClickOnTShirtTab()
	{
		aphomepage.clickOn_TShirtTab();
	}
	
}