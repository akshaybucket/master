package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;

public class HomePageSteps {
	
	TestContext testContext;
	HomePage homePage;
	
	public HomePageSteps(TestContext context) {
		testContext = context;
		homePage = testContext.getPageObjectManager().getHomePage();
	}
	
	@Given("^user is on Home Page$")
	public void user_is_on_Home_Page(){
		homePage.navigateTo_HomePage();	
	}
	
	@Given("^I am on Home Page$")
	public void I_am_on_Home_Page(){
		homePage.navigateTo_HomePage();	
	}

	@Given("^I click on women tab$")
	public void i_click_on_dresses_tab() throws Throwable {
		homePage.navigateTo_WomenTab();
	}
	
	@When("^I add product to cart and proceed$")
	public void i_add_product_to_cart_and_proceed() throws Throwable {
		homePage.addProductToCartAndProceed();
	}
	
	@Given("^I enter username and password and clicks on sign-in button$")
	public void EnterCredentialsAndLogin(){
		homePage.Loginwithcredentials();	
	}
	
	@When("^he search for \"([^\"]*)\"$")
	public void he_search_for(String product)  {
		homePage.perform_Search(product);
	}
	
	/*@Then("^Shopping cart Summary page should be displayed$")
	public void shopping_cart_Summary_page_should_be_displayed() throws Throwable {
	  homePage.AssertShoppingCartSummaryPage();
	}*/

	@When("^he clicks on Mens tab$")
	public void he_clicks_on_Mens_tab(){
		homePage.clickOn_MensTab();
	}
}