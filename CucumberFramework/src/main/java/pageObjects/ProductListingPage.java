package pageObjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import selenium.Wait;

public class ProductListingPage {
	WebDriver driver;
	
	public ProductListingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.CSS, using = "button.single_add_to_cart_button") 
	private WebElement btn_AddToCart;
	
	@FindAll(@FindBy(how = How.CSS, using = ".noo-product-inner"))
	private List<WebElement> prd_List;	
	
	@FindBy(how = How.XPATH, using = "//h4[text()='Product Categories']/following::a[text()='Pants & Shirts']")  
	private WebElement pants_shirts_filter;
	
	
	public void clickOn_AddToCart() {
		btn_AddToCart.click();
		Wait.untilJqueryIsDone(driver);
	}
	
	public void select_Product(int productNumber) {
		prd_List.get(productNumber).click();
	}
	
	public String getProductName(int productNumber) {
		return prd_List.get(productNumber).findElement(By.cssSelector("h3")).getText();
	}

	public void clickOn_PantsShirts(){
		Wait.untilJqueryIsDone(driver);
		pants_shirts_filter.click();	
		Wait.untilJqueryIsDone(driver);
	}
	
	public void selectColor(String color)
	{
		new Select(driver.findElement(By.xpath("//select[@name='filter_color']"))).selectByVisibleText(color);
	}
	
	public void selectSize(String size)
	{
		new Select(driver.findElement(By.xpath("//select[@name='filter_size']"))).selectByVisibleText(size);
	}
}