package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import org.testng.Assert;
import managers.FileReaderManager;
import selenium.Wait;

public class HomePage {
	WebDriver driver;

	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//a[text()='Man']") 
	private WebElement mens_tabs;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Women']") 
	private WebElement women_tab;
	
	@FindBy(how = How.XPATH, using = "//img[@title='Faded Short Sleeve T-shirts']") 
	private WebElement firstProduct;
	
	@FindBy(how = How.XPATH, using = "//p[@id='add_to_cart']") 
	private WebElement addToCartButton;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Proceed to checkout']") 
	private WebElement proceedToCheckoutButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='navigation_page']") 
	private WebElement SummaryPageTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@id='email']") 
	private WebElement username_field;
	
	@FindBy(how = How.XPATH, using = "//input[@id='passwd']") 
	private WebElement password_field;
	
	@FindBy(how = How.XPATH, using = "//button[@name='SubmitLogin']") 
	private WebElement SignIn_button;
	
	public void perform_Search(String search) {
		driver.navigate().to(FileReaderManager.getInstance().getConfigReader().getApplicationUrl() + "/?s=" + search + "&post_type=product");
	}

	public void navigateTo_HomePage() {
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
	}
	
	public void navigateTo_WomenTab()
	{
		women_tab.click();
	}

	public void addProductToCartAndProceed()
	{
		firstProduct.click();

		Wait.untilJqueryIsDone(driver);
		
		driver.switchTo().frame(0);
		addToCartButton.click();
		
		Wait.untilJqueryIsDone(driver);
		proceedToCheckoutButton.click();
	}
	
	/*public void	AssertShoppingCartSummaryPage()
	{
		Assert.assertEquals("Your shopping cart ", SummaryPageTitle.getText());
	}*/
	
	public void Loginwithcredentials() {
		username_field.sendKeys(FileReaderManager.getInstance().getConfigReader().getUsername());
		password_field.sendKeys(FileReaderManager.getInstance().getConfigReader().getPassword());
		
		SignIn_button.click();
		
	}
	
	public void clickOn_MensTab(){
		mens_tabs.click();
		Wait.untilJqueryIsDone(driver);
	}
}