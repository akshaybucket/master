package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import managers.FileReaderManager;
import selenium.Wait;

public class APHomePage {
	WebDriver driver;

	public APHomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//a[text()='T-shirts']") 
	private WebElement tshirt_tab;

	
	public void clickOn_TShirtTab(){
		tshirt_tab.click();
		Wait.untilJqueryIsDone(driver);
	}
}