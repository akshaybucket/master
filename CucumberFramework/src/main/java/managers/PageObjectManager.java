package managers;

	import org.openqa.selenium.WebDriver;

import pageObjects.APHomePage;
import pageObjects.APLoginPage;
	import pageObjects.CartPage;
	import pageObjects.CheckoutPage;
	import pageObjects.ConfirmationPage;
	import pageObjects.HomePage;
	import pageObjects.ProductListingPage;

public class PageObjectManager {
	private WebDriver driver;
	private ProductListingPage productListingPage;
	private CartPage cartPage;
	private HomePage homePage;
	private CheckoutPage checkoutPage;
	private ConfirmationPage confirmationPage;
	private APLoginPage apLoginPage;
	private APHomePage aphomePage;
	
	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}
	
	public HomePage getHomePage(){
		return (homePage == null) ? homePage = new HomePage(driver) : homePage;
	}
	
	public APHomePage getAPHomePage(){
		return (aphomePage == null) ? aphomePage = new APHomePage(driver) : aphomePage;
	}
	
	public ProductListingPage getProductListingPage() {
		return (productListingPage == null) ? productListingPage = new ProductListingPage(driver) : productListingPage;
	}
	
	public CartPage getCartPage() {
		return (cartPage == null) ? cartPage = new CartPage(driver) : cartPage;
	}
	
	public CheckoutPage getCheckoutPage() {
		return (checkoutPage == null) ? checkoutPage = new CheckoutPage(driver) : checkoutPage;
	}
	
	public ConfirmationPage getConfirmationPage() {
		return (confirmationPage == null) ? confirmationPage = new ConfirmationPage(driver) : confirmationPage;
	}
	
	public APLoginPage getAPLoginPage(){
		return (apLoginPage == null) ? apLoginPage = new APLoginPage(driver) : apLoginPage;
	}
}